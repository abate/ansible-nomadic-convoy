#!/bin/bash

set -e

DESTINATION=$1
CONVOY="convoy"
VOLUMES=$($CONVOY list | jq 'keys | .[]' | uniq | sed -e 's/^"//' -e 's/"$//')

log () {
  LOGDATE=$(date --utc +"%Y-%m-%dT%H:%M:%S.%3NZ")
  echo "$LOGDATE $1"
}

log "Starting to backup..."

for VOLUME in $VOLUMES; do
  log "Backing up $VOLUME..."
  DATE=$(date --utc +"%Y%m%d%H%M%S")
  SNAPSHOT="${VOLUME}_${DATE}"

  log "$VOLUME: creating snaphot $SNAPSHOT..."
  $CONVOY snapshot create "$VOLUME" --name "$SNAPSHOT"

  log "$VOLUME: moving $SNAPSHOT to $DESTINATION..."
  $CONVOY backup create "$SNAPSHOT" --dest "$DESTINATION"

  log "$VOLUME: removing $SNAPSHOT..."
  $CONVOY snapshot delete "$SNAPSHOT"

  log "$VOLUME: backup done!"
done

log "Done with all backups!"
