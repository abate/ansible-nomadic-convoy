# ansible-role-convoy


### Usage


    - name: Playbook
      roles:
        - role: ansible-role-convoy

Use the variable `convoy_options` to pass the convoy driver to the daemon.

Ex.: To use the `vfs` driver :

      convoy_options: "--drivers vfs --driver-opts vfs.path=/home/convoy"

To use a `devicemapper` (recommended)

      convoy_options: "--drivers devicemapper --driver-opts dm.datadev=/dev/storage/convoy_data --driver-opts dm.metadatadev=/dev/storage/convoy_metadata --driver-opts dm.defaultvolumesize=10G --driver-opts dm.fs=xfs"

### Include in requirements

### Variables

| Variable Name          | Default Value | Example |
| ---------------------- | ------------- | ------- |
| docker_convoy_version  | 0.5.0         | 0.4.2   |
